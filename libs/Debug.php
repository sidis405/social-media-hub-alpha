<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Library
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2012 BCLab Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */


/**
 * The Debug Class handles some debug data to be gatheres and send back to the controller that calls it
 * 
 * @author Sidrit Trandafili  
 */
class Debug {

    /**
     * Prints out a 'pre' section containing defined data
     * 
     * @param <array> $instance The debug array
     * @return void
     */
    public static function debugData($instance) {
        /**
         * If val==true means we should print the [data] element using the [printType function] (this last one is eithe print_r or var_dump for google-prettify
         */
        if (isset($instance['val']) && $instance['val'] === true) {
            print "<pre id='debug_data'>";
            $instance['printType']($instance['data']);
            print "</pre>";
            print "<pre><button class='btn btn-danger'  id='debug_data_toggle'>Toggle Debug Data</button></pre>";
        }
    }

     /**
     * Prints out an info item
      * 
     * @param <string> $relName Fieldname
     * @param <string> $data Data to print
     * @return void
     */
    public static function infoItem($relName, $data) {

        print "<a href=\"#\" id=\"example\" class=\"btn btn-danger infoBed\" rel=\"popover\" data-content=\"$data\" data-original-title=\"Info\"><i class='icon-info-sign icon-white'></i></a>";
    }

    
     /**
     * Magic Method __call
     * @param <string> $name Called non-existing method
     * @param <array> $arguments Params
     * @throws Exception
     */
    public function __call($name, $arguments) {
        throw new Exception($this->_callingController . " : The method $name with arguements :" . print_r($arguments) . " does not exist");
    }

    /**
     * Magic Method __get
     * @param type $name Non existing property
     * @throws Exception
     */
    public function __get($name) {
        throw new Exception($this->_callingController . " : The property $name does not exist");
        
    }
    
    /**
     * Magic Method __set
     * @param type $name Non existing property
     * @param type $value Value
     * @throws Exception
     */

    // public function __set($name, $value) {
    //     throw new Exception($this->_callingController . " : You were going to set the property $name with value $value but it does not exist");
    // }
    
}
