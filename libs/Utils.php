<?php

/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Library
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2012 BCLab Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * Description of class Utils : Class with static methods to use round the application. Much like Helpers
 * 
 * @author Sidrit Trandafili 
 */
class Utils {

    /**
     * Description of function dayOrdinal : Give ordinal values of received date
     * 
     * @param <string> $date Date on yyy-mm-dd mysql format
     * @return <string>
     */
    public static function dayOrdinal($date) {

        /**
         * Split the date on "-"
         */
        list($year, $month, $day) = explode("-", $date);

        /**
         * If the find 1, 2, 3 as second or only digit excluding the 11th, 12th or 13th do wht switch and append
         */
        if (!in_array(($day % 100), array(
                    11,
                    12,
                    13
                ))) {
            switch ($day % 10) {

                // Handle 1st, 2nd, 3rd

                case 1:
                    return $day . '<sup>st</sup>';

                case 2:
                    return $day . '<sup>nd</sup>';

                case 3:
                    return $day . '<sup>rd</sup>';
            }
        }

        /**
         * Return ordinal value of date
         */
        return $day . '<sup>th</sup>';
    }

    public static function sendEmail($to, $subject, $body, $additional_data = false) {
/*
        require 'Mail.php';
        
        //$mail_instance = new Mail();

        $headers = array('From' => EMAIL_FROM_SYSTEM,
            'To' => $to,
            'Subject' => $subject);

        @$mail = Mail::factory('smtp', array('host' => EMAIL_HOST,
                    'port' => EMAIL_PORT,
                    'auth' => true,
                    'username' => EMAIL_USERNAME,
                    'password' => EMAIL_PASSWORD));
        
        
        if($additional_data){
            
            
            $body .= "\n\nThat happened on page '" . $_SERVER['PHP_SELF'] . "' from IP Address '" . $_SERVER['REMOTE_ADDR'] . ":" . $_SERVER['REMOTE_PORT'] . "' coming from the page (referrer) '" . $_SERVER['HTTP_REFERER'] . "'.\n\n";
            $body .= "\n\nThe user agent given was '" . $_SERVER['HTTP_USER_AGENT'] . "'.\n\n";

            
        }
        


        @$send = $mail->send($to, $headers, $body);
*/
//        if (@PEAR::isError($send)) {
//            echo("<pre>" . $send->getMessage() . "</pre>");
//        } else {
//            echo("<pre>Message successfully sent!</pre>");
//        }
    }



    public static function useCache($cacheFilename, $cacheDirectory, $cacheAge, $callback_object, $callback_function, $callback_parameter, $noCache) {

        $aCache = new QuickCache();
        $aCache->SetCacheFileName($cacheFilename);
        $aCache->SetCacheDirectory($cacheDirectory);
        $aCache->SetCacheMaxAgeInMins($cacheAge);

        //print_r($callback_parameter);

        if($noCache == false){

        if (($aCache->IsCacheTooOld()) || (!$aCache->DoesCacheAlreadyExist())) {

            $result = call_user_func_array(array($callback_object, $callback_function), $callback_parameter);


            $aCache->AddToCache(serialize($result));
        }
        
        }else{
            
            
             $result = call_user_func_array(array($callback_object, $callback_function), $callback_parameter);
            
             // $aCache->AddToCache(serialize($result));

        }

        $data = unserialize($aCache->RetrieveFromCache());


        // print_r($data);

        return $data;
    }
    

    public static function image_cache($full_filename, $cache_location, $cache_out_location, $redirected = 0){


        


        $filename_pieces = explode('/', $full_filename);


        if($redirected == 1){


            $filename = $filename_pieces[count($filename_pieces)-2] . '.jpg';

        }else{

            $filename = $filename_pieces[count($filename_pieces)-1];

        }

        $filename_cache = $cache_location.'/'.$filename;

        // print "$filename_cache<br>";

        if(file_exists($filename_cache)){

            return Utils::string_image_cache($cache_out_location.$filename);

        }else{

            if($redirected == 1){

            @$temp=file_get_contents($full_filename);

            $full_filename = json_decode($temp)->data->url;

        }


            @$image = file_get_contents($full_filename);

            $fp = fopen($filename_cache, "w");

            if(!fwrite($fp, $image)){
            }

        fclose($fp);

            return Utils::string_image_cache($cache_out_location.$filename);

        }
}

    public static function string_image_cache($string){

        $pieces = explode("/", $string);

        return base64_encode(join("/", array($pieces[count($pieces)-3], $pieces[count($pieces)-2], $pieces[count($pieces)-1])));

    }


    public static function loadModel($string){
        

        include_once CACHE_BASE.'/'.MODELS.strtolower($string).".php";


    }


    /**
     * Magic Method __call
     * @param <string> $name Called non-existing method
     * @param <array> $arguments Params
     * @throws Exception
     */
    public function __call($name, $arguments) {
        throw new Exception($this->_callingController . " : The method $name with arguements :" . print_r($arguments) . " does not exist");
    }

    /**
     * Magic Method __get
     * @param type $name Non existing property
     * @throws Exception
     */
    public function __get($name) {
        throw new Exception($this->_callingController . " : The property $name does not exist");
    }

    /**
     * Magic Method __set
     * @param type $name Non existing property
     * @param type $value Value
     * @throws Exception
     */
    public function __set($name, $value) {
        throw new Exception($this->_callingController . " : You were going to set the property $name with value $value but it does not exist");
    }

}
