<?php
//	This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License.
//	To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
	
	define('QUICKCACHE_DIRECTORY', 'cache');
	define('QUICKCACHE_MAX_AGE_MINS', 60);
	
	class QuickCache
	{	
		private $oCacheFileName = '';	
		private $oCacheDirectory = QUICKCACHE_DIRECTORY;
		private $oCacheMaxAgeMins = QUICKCACHE_MAX_AGE_MINS;
		private $oCacheReady = false;
                private $aSafeName;
		
		function __construct()
		{
			$this->oCacheDirectory = $this->oCacheDirectory . '/';
		}
		
		// Use this function to set the filename for the cache and set the cache as ready to use or not
		public function SetCacheFileName($aCacheFileName)
		{
			$this->SanitiseCacheName($aCacheFileName);
			$this->SetCacheReady();
		}
		
		// Set whether the Cache is ready to use or not
		private function SetCacheReady()
		{
			$this->oCacheReady = false;
			
			// We are only ready to roll if we have a valid filename and directory to use
			if (($this->oCacheFileName != '') && (is_dir($this->oCacheDirectory))) $this->oCacheReady = true;
		}
		
		// Use this function to return whether the cache is ready to use or not
		private function IsCacheReady()
		{
			return $this->oCacheReady;
		}
		
		// Determine whether the cache already exists or not by checking the cache is ready and the cache file exists
		public function DoesCacheAlreadyExist()
		{
			$aExists = false;
			
			if (($this->IsCacheReady()) && (file_exists($this->oCacheDirectory . $this->oCacheFileName))) $aExists = true;

			return $aExists;
		}  
		
		// Helper function to sanitise the name of the cache file so that we can guarantee it can be saved
		private function SanitiseCacheName($aCacheFileName)
		{		
			// Lower case it first and replace spaces with underscores
			$aCacheFileName = strtolower($aCacheFileName); 
			$aCacheFileName = str_replace(" ", "", $aCacheFileName); 
		
			// Loop through string removing odd chars to be safe
			$aSafeVersion = ''; 
			for ($aIndex = 0; $aIndex < strlen($aCacheFileName); $aIndex++) 
			{ 
				if (preg_match('([0-9]|[a-z]|_)', $aCacheFileName[$aIndex])) 
				{
					@$aSafeName .= $aCacheFileName[$aIndex];
				} 
			} 
		
			$this->oCacheFileName = $aSafeName; 
		}
				
		// Use this function to change the default caching directory - no final / please!
		public function SetCacheDirectory($aDirectory)
		{
			$aSuccess = false;
			
			if (is_dir($aDirectory)) 
			{
				$this->oCacheDirectory = $aDirectory . '/';
				$this->SetCacheReady();
				$aSuccess = true;
			}
			
			return $aSuccess;
		}
		
		// Use this function to set the max age of the cache in minutes
		public function SetCacheMaxAgeInMins($aMaxNumberOfMins)
		{
			$aSuccess = true;
			
			if (is_numeric($aMaxNumberOfMins)) $this->oCacheMaxAgeMins = $aMaxNumberOfMins;
			else $aSuccess = false;
			
			return $aSuccess;
		}
		
		// Use this function to retrieve content from the cache
		public function RetrieveFromCache()
		{
			$aContent = '';						
			
			if ($this->DoesCacheAlreadyExist()) $aContent = file_get_contents($this->oCacheDirectory . $this->oCacheFileName);
			
			return $aContent;
		}
		
		// Use this function to add content to the cache
		public function AddToCache($aCacheContent)
		{
			$aSuccess = true;
						
			if (($this->IsCacheReady()) && ($aCacheContent != '')) $aSuccess = (file_put_contents($this->oCacheDirectory . $this->oCacheFileName, $aCacheContent) !== false);
			else $aSuccess = false;
			
			return $aSuccess;
		}
		
		// Use this function to check if the content of the cache is over the maximum age allowed - if so then you need to reset the cache contents
		public function IsCacheTooOld()
		{
			$aTooOld = false;
			
			if ($this->DoesCacheAlreadyExist())
			{
				$aModifiedTime = filemtime($this->oCacheDirectory . $this->oCacheFileName);
				$aCurrentTime = mktime();
				
				if ($aCurrentTime - $aModifiedTime >= $this->oCacheMaxAgeMins * 60) $aTooOld = true;				
			}
			
			return $aTooOld;
		}
	}
?>