<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Library
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2012 BCLab Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */


/**
 * The View Class
 * 
 * @param <string> $_callingController : A variable to store the controller that instantiated this view object
 * @author Sidrit Trandafili
 */
class View {
    
    public $_callingController;
    public $js;
    public $dataSet;
    public $debug;
    public $msg;
    /**
     * Instantiates the object and creates the js property as an array
     * 
     * @param  $callingController
     * @return 
     */
    function __construct($callingController) {
        
        /**
         * Assign the $callingController received fromt he main controller and assign it to $_callingController
         */
        $this->_callingController = strtolower($callingController);
        /**
         * Create the js array to include in the view header
         */
        $this->js = array();
        
    }
    
    /**
     * Renders , displays the view
     * 
     * @param <string> $name Name of the view file
     * @param <bool> $noInclude[optional] default value : false This defines if the header and footer should be left out. Handy if we need to make ajax calls to pages and get only the relevant page html
     * @return void
     */
    public function render($name, $noInclude = false) {
        /**
         * Add the js file with the controller name to the js array to send to the header
         */
        array_push($this->js, $this->_callingController);
        /**
         * if instructed to not include leave heder and footer out
         */
        if ($noInclude == true) {
            require 'views/' . $this->_callingController . '/' . $name . '.php';
            
        }
        
        else {
            require 'views/header.php';
            require 'views/' . $this->_callingController . '/' . $name . '.php';
            require 'views/footer.php';
            
        }
        
    }
    
    
     /**
     * Magic Method __call
     * @param <string> $name Called non-existing method
     * @param <array> $arguments Params
     * @throws Exception
     */
    public function __call($name, $arguments) {
        throw new Exception($this->_callingController . " : The method $name with arguements :" . print_r($arguments) . " does not exist");
    }

    /**
     * Magic Method __get
     * @param type $name Non existing property
     * @throws Exception
     */
    public function __get($name) {
        throw new Exception($this->_callingController . " : The property $name does not exist");
        
    }
    
    /**
     * Magic Method __set
     * @param type $name Non existing property
     * @param type $value Value
     * @throws Exception
     */

    // public function __set($name, $value) {
    //     throw new Exception($this->_callingController . " : You were going to set the property $name with value $value but it does not exist");
    // }
    
    
}
