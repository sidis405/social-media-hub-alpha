<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Library
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2012 BCLab Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * HASHING Class
 * 
 * @author Sidrit Trandafili
 */
class Hash {
    
    /**
     * Hashes a string
     * 
     * @param <string> $algo The algorithm (md5, sha1, whirlpool, etc)
     * @param <string> $data The data to encode
     * @param <string> $salt The salt (This should be the same throughout the system probably)
     * @return <string> The hashed/salted data
     */
    public static function create($algo, $data, $salt) {
        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context, $data);
        return hash_final($context);
        
    }
    
     /**
     * Magic Method __call
     * @param <string> $name Called non-existing method
     * @param <array> $arguments Params
     * @throws Exception
     */
    public function __call($name, $arguments) {
        throw new Exception($this->_callingController . " : The method $name with arguements :" . print_r($arguments) . " does not exist");
    }

    /**
     * Magic Method __get
     * @param type $name Non existing property
     * @throws Exception
     */
    public function __get($name) {
        throw new Exception($this->_callingController . " : The property $name does not exist");
        
    }
    
    /**
     * Magic Method __set
     * @param type $name Non existing property
     * @param type $value Value
     * @throws Exception
     */

    public function __set($name, $value) {
        throw new Exception($this->_callingController . " : You were going to set the property $name with value $value but it does not exist");
    }
    
    
}
