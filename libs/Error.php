<?php

class Error extends Controller {

    public $_msg;
    
    function __construct($msg) {
        parent::__construct();
        
        $this->_msg = $msg;
        
    }

    function index() {
        
        $this->view->msg = $this->_msg;
        $this->view->render('index');
    }

}