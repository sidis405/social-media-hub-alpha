<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Library
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2012 BCLab Sidrit Trandafili
 * @license http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * The Bootstrap class is called from the index.php page and handles routing in combination with the .htaccess file to the controller to send to the view
 * 
 * @author Sidrit Trandafili  
 */
class Bootstrap {

    /**
     * Instantiates the object, parses the url, defines if there is a controller, if not fallsback on a defined one.
     * instantiates the controller, sees if there is a method  requested, it instantiates that and handles errors too
     * 
     * @param <string> $url The requested URL
     * @param <object> $controller The Instantiated controller
     * @param <string> $file The full filename of the controller to request for instatiation
     * @return void 
     */
    
    function __construct() {
        
        /**
         * Fetch the requested URL from the $_GET array, remove trailing slash, salitize it to avoi the NULL BYTE exploit and split it on forward slash
         * so we can get the controller name and the requested method if requested
         */
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);

        /**
         * If the url is empty , probably index page, fallback on a predefined controller and stop execution
         * maybe move the defined controller into the config file 
         */
        if (empty($url[0])) {
            
            require 'controllers/index.php';
            
            $controller = new Index();
            $controller->loadModel("index");
            
            $controller->index();
            return false;
        }
        
        /**
         * We've estabilished that the element [0] of the url is the controller name
         * Create a path for as the filename
         */
        $file = 'controllers/' . $url[0] . '.php';
        
        /**
         * We check if the file exists, if so we require it, if not we throw an error
         */
        if (file_exists($file)) {
            require $file;
        } else {
            $this->error();
            return;
        }
        
        /**
         * We instantiate the Controller
         */
        $controller = new $url[0];
        
        /**
         * We request the appropriate model for it
         */
        $controller->loadModel($url[0]);

        /**
         * We check if parameters have been passed to the method
         * so the url array is
         * 0 - controller
         * 1 - method
         * 2 - params
         */
        
        try {
        
        if (isset($url[2])) {
            /**
             * If params have been passed, we check that the specific method exists. if so we call it and send params. If not we throw an error
             */
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            } else {
                $this->error();
            }
        } else {
            
            /**
             * If no params have been passed wi check that the method exists and call it with no params . if not we throw an error
             */
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    //$this->error();
                }
                /**
                 * if no methods have been requested we fall back on INDEX method
                 */
            } else {
                $controller->index();
            }
        }
        } catch (Exception $e) {
            
            $this->catchException($e->getMessage());
        }
    }

    /**
     * Instantiates the Error object and calls the index() method of it
     * 
     * @return void 
     */
    function error() {
       // require 'controllers/error.php';
        $controller = new Error("This page does not exist");
        $controller->index();
        return false;
    }
    
    function catchException($e_msg){
        
        $controller = new Error($e_msg);
        $controller->index();
        return false;
        
    }
    
     /**
     * Magic Method __call
     * @param <string> $name Called non-existing method
     * @param <array> $arguments Params
     * @throws Exception
     */
    public function __call($name, $arguments) {
        throw new Exception($this->_callingController . " : The method $name with arguements :" . print_r($arguments) . " does not exist");
    }

    /**
     * Magic Method __get
     * @param type $name Non existing property
     * @throws Exception
     */
    public function __get($name) {
        throw new Exception($this->_callingController . " : The property $name does not exist");
        
    }
    
    /**
     * Magic Method __set
     * @param type $name Non existing property
     * @param type $value Value
     * @throws Exception
     */

    public function __set($name, $value) {
        throw new Exception($this->_callingController . " : You were going to set the property $name with value $value but it does not exist");
    }
    

}
