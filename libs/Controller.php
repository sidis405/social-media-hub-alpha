<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Library
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2012 BCLab Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * This is the Controller Class.
 * 
 * @param <string> $_callingController The child controller that called this controller by extending and running the __construct on it's own __constreuct
 * @author Sidrit Trandafili  
 */
class Controller {
    
    public $_callingController;
    
    public $view;
    
    public $model;
    
    /**
     * Instantiates the object and the view object. Passes the $_callingController as a foldername for inclusion
     * 
     * @param
     * @return void 
     */
    function __construct() {
        
        /**
         * Get the name of the child controller class that called this construct
         */
        $this->_callingController = get_class($this);
        /**
         * Pass the name of the child controller class to the view
         */
        $this->view = new View($this->_callingController);
        
        
        
        
        
        /**
         * Require error controller
         */
        
//        require 'controllers/error.php';
        
    }
    
    /**
     * receives a controlelr name as value, appends '_model.php' and instantiates the model This is called in the Bootstrap
     * 
     * @param <string> $name The name of the controller
     * @return 
     */
    public function loadModel($name) {
        
        /**
         * Create a path as the filename for the model
         */
        $path = 'models/' . $name . '_model.php';

        // print $path;
        
        /**
         * Check if exists. If it does include and create an instance
         */
        if (file_exists($path)) {
            require 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
            
        }
        
    }
    
    /**
     * Magic Method __call
     * @param <string> $name Called non-existing method
     * @param <array> $arguments Params
     * @throws Exception
     */
    public function __call($name, $arguments) {
        throw new Exception($this->_callingController . " : The method $name with arguments :" . print_r($arguments) . " does not exist");
    }

    /**
     * Magic Method __get
     * @param type $name Non existing property
     * @throws Exception
     */
    public function __get($name) {
        throw new Exception($this->_callingController . " : The property $name does not exist");
        
    }
    
    /**
     * Magic Method __set
     * @param type $name Non existing property
     * @param type $value Value
     * @throws Exception
     */

    // public function __set($name, $value) {
    //     throw new Exception($this->_callingController . " : You were going to set the property $name with value  but it does not exist");
    // }
    
    
}
