<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Models
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2013 The Yellow Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * The Index_Model Class makes the request to the API, elaborates the output and sends it back
 *       to the controller to send to the view
 * 
 * @author Sidrit Trandafili  
 */

class Twitter_Model extends Model {

    /**
     * Instantiates the object and calls the constructor of the Parent class
     * 
     * @param
     * @return void 
     */
    


    function __construct() {
        parent::__construct();



    }

   	

	function twitter_cmp_by_createdTime($itemA, $itemB) {
  		
		// print_r($itemA); die;

  		return strtotime($itemB['date_created']) - strtotime($itemA['date_created']);

		}


    function twitter_getStreams($filter = '0'){

      $out = array();

      $memcache = new Memcache;
      $cacheAvailable = $memcache->connect(MEMCACHED_HOST, MEMCACHED_PORT);

    $items_array = unserialize(TWITTER_ITEMS);


      if($filter !== '0'){

        $chosen_item_array = array(

          $filter => $items_array[$filter]
            );

      }else{

        $chosen_item_array = $items_array;

      }



      foreach ($chosen_item_array as $key => $this_item) {

       $key = "twitter_" . $this_item['type'] . "_" . $filter;

      if($this_item['type'] == 'tag'){



        if ($cacheAvailable == true && MEMCACHED_USE){

          $result = $memcache->get($key);

        }


        if(@!$result){

          $result = $this->twitter_getStreams_tags($this_item);
          $memcache->set($key, $result);

        }


        

        $out = array_merge($out, $result);

      }elseif($this_item['type'] == 'user'){


        if ($cacheAvailable == true && MEMCACHED_USE){

          $result = $memcache->get($key);

        }


        if(@!$result){

          $result = $this->twitter_getStreams_users($this_item);
          $memcache->set($key, $result);

        }
        $out = array_merge($out, $result);
      }


    }



    usort($out, array($this, "twitter_cmp_by_createdTime"));

    $out = array_unique($out, SORT_REGULAR);


    return $out;

    }



	function twitter_getStreams_users($current_item){

    $out = array();



			$stream = json_decode(file_get_contents("http://api.twitter.com/1/statuses/user_timeline.json?screen_name=".$current_item['value']."&count=32000&page=1%27"));
		
      foreach ($stream as $this_item) {

        

      	 @$this_item_parsed = array(

            'source' => 'Twitter',
            'type' => 'post',
            'item_id' => '',
            'date_created' => $this_item->created_at,
            'text' => $this_item->text,
            'item_text' => '',
            'picture' => '',
            'url' => '',
            'from' => array(
              'user_id' => $this_item->user->id,
              'user_name' => $this_item->user->screen_name,
              'user_fullname' => $this_item->user->name,
              'user_profile_picture' => Utils::image_cache($this_item->user->profile_image_url, TWITTER_CACHE_PROFILE, TWITTER_CACHE_PROFILE_OUT)
              )
            );
         array_push($out, $this_item_parsed);
		}

		return $out;
	}	

  function twitter_getStreams_tags($current_item){

    $out = array();



      $stream = json_decode(file_get_contents("http://search.twitter.com/search.json?q=".$current_item['value']));
    



      foreach ($stream->results as $this_item) {

        @$this_item_parsed = array(

            'source' => 'Twitter',
            'type' => 'post',
            'item_id' => '',
            'date_created' => $this_item->created_at,
            'text' => $this_item->text,
            'item_text' => '',
            'picture' => '',
            'url' => '',
            'from' => array(
              'user_id' => $this_item->from_user_id,
              'user_name' => $this_item->from_user,
              'user_fullname' => $this_item->from_user_name,
              'user_profile_picture' => Utils::image_cache($this_item->profile_image_url, TWITTER_CACHE_PROFILE, TWITTER_CACHE_PROFILE_OUT)
              )
            );

			array_push($out, $this_item_parsed);
      }
   


    return $out;
  } 


// function twitter_getStreams_tags_cache($filter = '0', $prefix){



//   $data = Utils::useCache(join('_',array($prefix, $filter)) , TWITTER_CACHE_DATA, TWITTER_CACHE_DATA_AGE, $this, "twitter_getStreams_tags", array($filter), "false");

//         return $data;


// }

// function twitter_getStreams_users_cache($filter = '0', $prefix){



//   $data = Utils::useCache(join('_',array($prefix, $filter)) , TWITTER_CACHE_DATA, TWITTER_CACHE_DATA_AGE, $this, "twitter_getStreams_users", array($filter), "false");

//         return $data;


// }




}
