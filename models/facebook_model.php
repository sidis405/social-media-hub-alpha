<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Models
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2013 The Yellow Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * The Index_Model Class makes the request to the API, elaborates the output and sends it back
 *       to the controller to send to the view
 * 
 * @author Sidrit Trandafili  
 */

class Facebook_Model extends Model {

    /**
     * Instantiates the object and calls the constructor of the Parent class
     * 
     * @param
     * @return void 
     */
    


    function __construct() {
        parent::__construct();
    }


    function facebook_cmp_by_createdTime($itemA, $itemB) {
      
    // print_r($itemA); die;

      return strtotime($itemB['date_created']) - strtotime($itemA['date_created']);

    }

   	function facebook_getStreams_wall($filter = '0'){

    $out = array();

     $memcache = new Memcache;
      $cacheAvailable = $memcache->connect(MEMCACHED_HOST, MEMCACHED_PORT);

		$items_array = unserialize(FACEBOOK_ITEMS);


      if($filter !== '0'){

        $chosen_item_array = array(

          $filter => $items_array[$filter]


          );

      }else{

        $chosen_item_array = $items_array;

      }



		foreach ($chosen_item_array as $key => $entry) {


       $key = 'faceoook_user_' . $entry['value'];

    if ($cacheAvailable == true && MEMCACHED_USE){

      $stream = $memcache->get($key);

    }

    if(@!$stream){

      $stream = json_decode(file_get_contents("https://graph.facebook.com/".$entry['value']."/feed?access_token=".FACEBOOK_TOKEN));
    

    $memcache->set($key, $stream);

    }


		
      foreach ($stream->data as $this_item) {

          
       
        if(@strlen($this_item->story) < 1 ){

        @$this_item_parsed = array(

            'source' => 'Facebook',
            'type' => $this_item->type,
            'item_id' => $this_item->id,
            'date_created' => $this_item->created_time,
            'text' => $this_item->message,
            'item_text' => $this_item->description,
            'picture' => Utils::image_cache($this_item->picture, FACEBOOK_CACHE_MEDIA, FACEBOOK_CACHE_MEDIA_OUT),
            'picture_big' => Utils::image_cache($this_item->picture, FACEBOOK_CACHE_MEDIA, FACEBOOK_CACHE_MEDIA_OUT),
            'url' => $this_item->link,
            'from' => array(
              'user_id' => $this_item->from->id,
              'user_name' => '',
              'user_fullname' => $this_item->from->name,
              'user_profile_picture' => Utils::image_cache("http://graph.facebook.com/".$this_item->from->id."/picture?redirect=false", FACEBOOK_CACHE_PROFILE, FACEBOOK_CACHE_PROFILE_OUT, 1)

              ),
            'to' => array(
              'user_id' => $this_item->to->data[0]->id,
              'user_name' => '',
              'user_fullname' => $this_item->to->data[0]->name,
              'user_profile_picture' => "http://graph.facebook.com/".$this_item->to->data[0]->id."/picture"
              ),
            'interactions' => array(
              'likes' => array(),
              'comments' => array(),
              'shares' => array()
              )
            );
          

          // @echo is_array($this_item->likes->data);

          foreach (@(array)$this_item->likes->data as $key => $likes) {
                
              // print_r($likes); die;


                array_push($this_item_parsed['interactions']['likes'], array(
                        'user_id' => $likes->id,
                        'user_name' => '',
                        'user_fullname' => $likes->name,
                        'user_profile_picture' => "http://graph.facebook.com/".$likes->id."/picture"

                  ));

                }


          foreach (@(array)$this_item->comments->data as $comments) {
                
                array_push($this_item_parsed['interactions']['comments'], array(
                        'text' => $comments->message,
                        'created_at' => $comments->created_time,
                        'user_id' => $comments->from->id,
                        'user_name' => '',
                        'user_fullname' => $comments->from->name,
                        'user_profile_picture' => "http://graph.facebook.com/".$comments->from->id."/picture"

                  ));

                }

                array_push($out, $this_item_parsed);
          }}

      
    }

    usort($out, array($this, "facebook_cmp_by_createdTime"));

    $out = array_unique($out, SORT_REGULAR);
    // array_push($out, $stream);
		return $out;
	}	

}
