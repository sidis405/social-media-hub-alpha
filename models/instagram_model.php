<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Models
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2013 The Yellow Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * The Index_Model Class makes the request to the API, elaborates the output and sends it back
 *       to the controller to send to the view
 * 
 * @author Sidrit Trandafili  
 */

class Instagram_Model extends Model {

    /**
     * Instantiates the object and calls the constructor of the Parent class
     * 
     * @param
     * @return void 
     */
    


    function __construct() {
        parent::__construct();
    }

   	function instagram_getStreams($filter = '0'){


   		$out = array();

       $memcache = new Memcache;
      $cacheAvailable = $memcache->connect(MEMCACHED_HOST, MEMCACHED_PORT);

   		include_once LIBS.'instaphp/instaphp.php';

  		

   		$api = Instaphp\Instaphp::Instance(INSTA_TOKEN);


   		$items_array = unserialize(INSTA_ITEMS);


   		if($filter !== '0'){

   			$chosen_item_array = array(

   				$filter => $items_array[$filter]


   				);

			}else{


				$chosen_item_array = $items_array;

			}




   		foreach ($chosen_item_array as $current_item) {


   		//$structure = array();	
	
		//-- Get the response for Popular media
		


    $key = 'instagram_' . $current_item['controller'] . '_' . $current_item['value'];

    if ($cacheAvailable == true && MEMCACHED_USE){

      $response = $memcache->get($key);

    }

    if(@!$response){

    $response = $api->$current_item['controller']->$current_item['method']($current_item['value'], $current_item['params']);

    $memcache->set($key, $response);

    }





		//$structure['label'] =  $this_item['label'];
		//$structure['data'] = array();

		

		
		if (empty($response->error)){
		    foreach ($response->data as $this_item){

		 


		    	@$this_item_parsed = array(

            'source' => 'Instagram',
            'type' => 'photo',
            'item_id' => $this_item->id,
            'date_created' => date('Y-m-d H:m:i', $this_item->created_time),
            'text' => empty($this_item->caption->text) ? 'Untitled': str_replace('#' , ' #', $this_item->caption->text),
            'item_text' => empty($this_item->caption->text) ? 'Untitled': str_replace('#' , ' #', $this_item->caption->text),
            'picture' => Utils::image_cache($this_item->images->thumbnail->url, INSTA_CACHE_MEDIA, INSTA_CACHE_MEDIA_OUT),
            'picture_big' => Utils::image_cache($this_item->images->standard_resolution->url, INSTA_CACHE_MEDIA, INSTA_CACHE_MEDIA_OUT),
            'url' => '',
            'from' => array(
              'user_id' => '',
              'user_name' => $this_item->caption->from->username,
              'user_fullname' => $this_item->caption->from->full_name,
              'user_profile_picture' => Utils::image_cache($this_item->caption->from->profile_picture, INSTA_CACHE_PROFILE, INSTA_CACHE_PROFILE_OUT)
              ),
            'interactions' => array(
              'likes' => array(),
              'comments' => array(),
              'shares' => array(),
              'tags' => $this_item->tags
              ),
            'location' => $this_item->location
            
            );

			
			 foreach (@(array)$this_item->likes->data as $likes) {
                
              // print_r($likes); die;


                array_push($this_item_parsed['interactions']['likes'], array(
                        'user_id' => '',
                        'user_name' => $likes->username,
                        'user_fullname' => $likes->full_name,
                        'user_profile_picture' => Utils::image_cache($likes->profile_picture, INSTA_CACHE_PROFILE, INSTA_CACHE_PROFILE_OUT)

                  ));

                }


             foreach (@(array)$this_item->comments->data as $comments) {
                
                array_push($this_item_parsed['interactions']['comments'], array(
                        'text' => $comments->text,
                        'created_at' => date('Y-m-d H:m:i', $comments->created_time),
                        'user_id' => '',
                        'user_name' => $comments->from->username,
                        'user_fullname' => $comments->from->full_name,
                        'user_profile_picture' => Utils::image_cache($comments->from->profile_picture, INSTA_CACHE_PROFILE, INSTA_CACHE_PROFILE_OUT)

                  ));

                }




		        
		    	array_push ($out, $this_item_parsed);

		        }
		    }
		    //$structure['response'] = $response;
		    //array_push ($out, $structure);
		}


		usort($out, array($this, "instagram_cmp_by_createdTime"));

		$out = array_unique($out, SORT_REGULAR);

		//print_r($out);

		//die;
		 return $out;
	}


	function instagram_cmp_by_createdTime($itemA, $itemB) {
  		
		// print_r($itemA); die;

  		return strtotime($itemB['date_created']) - strtotime($itemA['date_created']);

		}


}
