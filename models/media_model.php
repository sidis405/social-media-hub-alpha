<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Models
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2013 The Yellow Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * The Index_Model Class makes the request to the API, elaborates the output and sends it back
 *       to the controller to send to the view
 * 
 * @author Sidrit Trandafili  
 */

class Media_Model extends Model {

    /**
     * Instantiates the object and calls the constructor of the Parent class
     * 
     * @param
     * @return void 
     */
    


    function __construct() {
        parent::__construct();
    }

   	function getMedia($media_id){


   		$out = array();

   		include_once LIBS.'instaphp/instaphp.php';

  		

   		$api = Instaphp\Instaphp::Instance(INSTA_TOKEN);

   		
		//-- Get the response for Popular media
		$response = $api->Media->Info($media_id);


    if (empty($response->error)){


      $item = $response->data;

      $this_item = array(

            'thumbnail' => array(
              'url' => Utils::image_cache($item->images->thumbnail->url, CACHE_MEDIA, CACHE_MEDIA_OUT),
              'width' => $item->images->thumbnail->width,
              'height' => $item->images->thumbnail->height,
              'caption' => empty($item->caption->text) ? 'Untitled':str_replace('#' , ' #', $item->caption->text),
              'url_big' => Utils::image_cache($item->images->standard_resolution->url, CACHE_MEDIA, CACHE_MEDIA_OUT),
              'url_width' => $item->images->standard_resolution->width,
              'url_height' => $item->images->standard_resolution->height
              ),
            'meta' => array(
              'media_id' => $item->id,
              'author' => array(
                'username' =>  $item->caption->from->username,
                'full_name' =>  $item->caption->from->full_name,
                'profile' =>  Utils::image_cache($item->caption->from->profile_picture, CACHE_PROFILE, CACHE_PROFILE_OUT)
                ),
              'caption' => empty($item->caption->text) ? 'Untitled': str_replace('#' , ' #', $item->caption->text),
              'tags' => $item->tags,
              'likes' => $item->likes->data,
              'comments' => $item->comments->data,
              'created_time' => $item->created_time
              )


            );

    }


		return $this_item;
	}
}
