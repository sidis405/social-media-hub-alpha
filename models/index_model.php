<?php
/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Models
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2013 The Yellow Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * The Index_Model Class makes the request to the API, elaborates the output and sends it back
 *       to the controller to send to the view
 * 
 * @author Sidrit Trandafili  
 */

class Index_Model extends Model {

    /**
     * Instantiates the object and calls the constructor of the Parent class
     * 
     * @param
     * @return void 
     */
    


    function __construct() {
        parent::__construct();
    }

   	function getStreams(){

   		Utils::loadModel("Facebook_Model");

        $facebook_model = new Facebook_Model();

        Utils::loadModel("Twitter_Model");

        $twitter_model = new Twitter_Model();

        Utils::loadModel("Instagram_Model");

        $instagram_model = new Instagram_Model();


        $out = array_merge(

        	$facebook_model->facebook_getStreams_wall(),
        	$twitter_model->twitter_getStreams(),
        	$instagram_model->instagram_getStreams()


        	);



		usort($out, array($this, "cmp_by_createdTime"));

		$out = array_unique($out, SORT_REGULAR);

		//print_r($out);

		//die;
		 return $out;
	}


	function cmp_by_createdTime($itemA, $itemB) {
  		
		// print_r($itemA); die;

  		return strtotime($itemB['date_created']) - strtotime($itemA['date_created']);

		}



}
