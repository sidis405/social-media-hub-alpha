
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


<?php
        


        if(strpos($_SERVER['QUERY_STRING'], 'media/index/')){

             $this_data = $this->dataSet;
        ?>

        <title><?= $this_data['thumbnail']['caption']; ?> @TheYellowRome | @TheYellowGuide Instagram Viewer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
       
        <meta name="author" content="Sidrit Trandafili">


        <meta property="fb:admins" content="710640073" />
        <meta name="twitter:card" content="photo">
        <meta name="twitter:site" content="@theyellowrome">
        <meta name="twitter:creator" content="@theyellowrome">
        <meta property="fb:app_id" content="491493840886506">
        <meta property="og:site_name" content="The Yellow Instagram Viewer | <?= $this_data['thumbnail']['caption']; ?>"/>
        <meta property="og:type" content="article"/>
        <meta property="og:description" content="<?= $this_data['thumbnail']['caption']; ?>">
        <meta property="og:url" content="<? print URL.str_replace("url=","",$_SERVER['QUERY_STRING']) ?>"/>
        <link rel="canonical" href="<? print URL.str_replace("url=","",$_SERVER['QUERY_STRING']) ?>"/>
        <meta property="og:title" content="@TheYellowRome | @TheYellowGuide Instagram Viewer"/>
        <meta property="og:image" content="<?= $this_data['thumbnail']['url_big']; ?>"/>
        <meta itemprop="name" content="<?= $this_data['thumbnail']['caption']; ?> @TheYellowRome | @TheYellowGuide Instagram Viewer"/>
        <meta name="description" content="<?= $this_data['thumbnail']['caption']; ?> @TheYellowRome | @TheYellowGuide Instagram Viewer"/>
        <meta itemprop="description" content="<?= $this_data['thumbnail']['caption']; ?> @TheYellowRome | @TheYellowGuide Instagram Viewer"/>

        <?
        }else{

            ?>

            <title>The Yellow | Insta Viewer</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
       
        <meta name="author" content="Sidrit Trandafili">


        <meta property="fb:admins" content="710640073" />
        <meta name="twitter:card" content="photo">
        <meta name="twitter:site" content="@theyellowrome">
        <meta name="twitter:creator" content="@theyellowrome">
        <meta property="fb:app_id" content="491493840886506">
        <meta property="og:site_name" content="The Yellow Instagram Viewer"/>
        <meta property="og:type" content="article"/>
        <meta property="og:description" content="@TheYellowRome | @TheYellowGuide Instagram Viewer">
        <meta property="og:url" content="http://insta.the-yellow.com/index"/>
        <link rel="canonical" href="http://web.stagram.com/p/388109802380124395_196373511"/>
        <meta property="og:title" content="@TheYellowRome | @TheYellowGuide Instagram Viewer"/>
        <meta property="og:image" content="http://insta.the-yellow.com/cache/profile/profile_196373511_75sq_1342707372.jpg"/>
        <meta itemprop="name" content="@TheYellowRome | @TheYellowGuide Instagram Viewer"/>
        <meta name="description" content="@TheYellowRome | @TheYellowGuide Instagram Viewer"/>
        <meta itemprop="description" content="@TheYellowRome | @TheYellowGuide Instagram Viewer"/>



<?


        }



?>
<!-- Le styles -->
    <link href="<?php echo URL; ?>public/css/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo URL; ?>public/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo URL; ?>public/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo URL; ?>public/css/custom.css" rel="stylesheet">
        <link href="<?php echo URL; ?>public/css/WebIconFonts-3.0/WebIconFonts-3.0.css" rel="stylesheet">





        <script src="<?php echo URL; ?>public/js/jquery.js"></script>
        
        <script src="<?php echo URL; ?>public/js/jquery.timeago.js"></script>
        <script src="<?php echo URL; ?>public/js/bootstrap.min.js"></script>
        
        <?php
        if(SWIPE !== false){

            print "<script src='".URL."public/js/bootstrap-carousel.js'></script>";

        }

            ?>

        <script src="<?php echo URL; ?>public/js/jquery.linkify.js"></script>


        

        <script src="<?php echo URL; ?>public/js/socialite.js"></script>

        

        <link href="<?php echo URL; ?>public/css/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo URL; ?>public/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <link href="<?php echo URL; ?>public/css/socialite.css" rel="stylesheet">

       

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo URL; ?>public/img/yellow_favicon.ico">

        
        <?php
//        if (isset($this->debug) && $this->debug === TRUE) {
//
//            print "<link href=\"" . URL . "public/prettify/prettify.css\" rel=\"stylesheet\">";
//        }

       

        

        ?>
        <style>
            body {
                padding-top: 60px; 
                /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>
        <link href="<?php echo URL; ?>public/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo URL; ?>public/css/fbish.css" rel="stylesheet">


        <link href="<?php echo URL; ?>public/css/skinv1/stylesheets/normalize.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL; ?>public/css/skinv1/stylesheets/all.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL; ?>public/css/skinv1/stylesheets/timeline.css" media="screen" rel="stylesheet" type="text/css" />
        <link href="<?php echo URL; ?>public/css/skinv1/stylesheets/fb-buttons.css" media="screen" rel="stylesheet" type="text/css" />
            <script src="<?php echo URL; ?>public/css/skinv1/javascripts/all.js" type="text/javascript"></script>




        <script type="text/javascript">


<?php

    if(TRACK === true){

?>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38446019-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

<?php


}
?>

  jQuery(document).ready(function() {
        jQuery("abbr.timeago").timeago();
    });

</script>

    </head>

    <body class='index'>


<div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="<?php echo URL; ?>"><i class='icon-blue'>The Yellow</i> | <i class='icon-red'>Social Media Hub</i></a>

                    <!-- <div class="fb-like pull-right span3" data-href="http://insta.the-yellow.com/index" data-send="true" data-width="450" data-show-faces="true"></div> -->

                    <div class="nav nav-pills nav-collapse">
                        <ul class="nav">

                            <?

                            foreach (unserialize(ENTITIES) as $entities) {
                                # code...
                            

                            ?>
                            <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href=""><span class="icon"><?= $entities['icon'];?></span> <?= $entities['text'];?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                            <?

                            print "<li><a href='".URL.$entities['main_controller']."'><span class='icon'>".$entities['icon']."</span> All</a></li>";

                            foreach (unserialize($entities['data']) as $this_item) {

                                $item_class = (strpos($_SERVER['QUERY_STRING'], $this_item['local_controller'].'/'.$this_item['search'])!==false) ? "active" : "" ;
                                print "<li class='$item_class'><a href='".URL.$this_item['local_controller'].'/'.$this_item['search']."'><i class='".$this_item['icon']." icon-red'></i>".$this_item['label']."</a></li>";
                                }


                            ?>

                        </ul></li>

                        <? } ?>

                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>



<script type="text/javascript">
    // alert(screen.width + ' x ' + screen.height);
</script>

        <div class="container" id='container'>

            <?php
            if (isset($this->debug))
                    Debug::debugData($this->debug); ?>