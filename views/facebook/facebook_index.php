<ol class="timeline clearfix">
  <li class="spine">
    <a href="#" title="Time"></a>
  </li>

<?php


foreach($this->dataSet as $this_data){
$entities = unserialize(ENTITIES);
print "
    <li class='story'>
    <i class='pointer'></i>
    <div class='unit'>
      
      <!-- Story -->
      <div class='storyUnit'>
        <div class='imageUnit'>
          <a href='#'><img class='img-rounded' src='".IMG.$this_data['from']['user_profile_picture']."' width='32' height='32' alt=''></a>
          <div class='imageUnit-content'>
            <h4><a href='#'>".$this_data['from']['user_fullname']."</a></h4>
            <p><abbr class=\"timeago\" title=\"".$this_data['date_created']."\" >" . $this_data['date_created'] . "</abbr>
            via ".$this_data['source']."</p>
          </div>
           <div class='sourceUnit'><h3><i class='".$entities[$this_data['source']]['icon-color']."'><span class='icon'>".$entities[$this_data['source']]['icon']."</span></i></h3></div>     
        </div>
      
        <p> ".$this_data['text']."</p>";

        if($this_data['type'] == 'photo'){


            print "<div class='photoUnit'>
          <img src='".IMG.$this_data['picture']."' height='403' width='403' alt='".$this_data['text']."'>
        </div>";


        }

      
      print "</div>
      <!-- / Story -->
      
      <!-- Units -->
      <ol class='storyActions'>
        <li><a href='#'>Like</a></li>
        <li><a href='#'>Comment</a></li>
         <li><a href='#'>Share</a></li>
        
      </ol>
      <!-- / Units -->
      
    </div>
  </li> ";






}

print "</ol>";
?>

</div>

<script type="text/javascript">


var left_column_height = 0;
var right_column_height = 0;
var items = $('.story');
for (var i = 0; i < items.length; i++) {
    
    /* this is purely to show vaird heights, the content would create the height...
    begin fpo: */
    // items.eq(i).height(Math.floor(Math.random() * 100) + 10);
    /* end fpo: */
    
    if (left_column_height > right_column_height) {
        right_column_height+= items.eq(i).addClass('right').outerHeight(true);
    } else {
        left_column_height+= items.eq(i).outerHeight(true);

    }
}


</script>
