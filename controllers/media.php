<?php

/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Controllers
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2013 The Ye Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * Index class handles interaction on landing page
 * 
 * @author Sidrit Trandafili  
 */
class Media extends Controller {

    /**
     * Instantiates the object and calls the constructor of the Parent class
     * 
     * @param
     * @return void 
     */
    function __construct() {
        parent::__construct();
        //Session::destroy();
    }

    /**
     * Sends the post array to the Model to elaborate data. Creates request, sends it to the model, parses response and send it to the view
     * 
     * @param <string> $dateStart The starting date for the search
     * @param <integer> $numNights The number of nights
     * @param <array> $request_params  Calling params to instatiate the HWApi from the model
     * @param <array> $data Response from the model (API response) ready to send to the view
     * @return void
     */
    function index() {


    	$url_pieces = explode("/", $_SERVER['REQUEST_URI']);

    	$media_id = $url_pieces[count($url_pieces)-1];

        /**
         * Call the model, send the instatiation data, and receive the array response from the server
         */
        

        $data = $this->model->getMedia($media_id);


        /**
         * Define the dataSet property to send to the VIew
         */
        $this->view->dataSet = $data;


        /**
         * Create an array and send to the Debug Property of the view. This will be checked and executed on the header.php file-
         */
        $this->view->debug = array(
            "val" => FALSE,
            "data" => $data,
            "printType" => "print_r"
        );

        /**
         * Call this particular method of the view
         */
        $this->view->render('index');
    }

}
