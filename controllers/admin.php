<?php

/**
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 * @category API Mobile Booking
 * @package Controllers
 * @author Sidrit Trandafili <forge405@gmail.com>
 * @copyright 2013 The Ye Sidrit Trandafili
 * @license	http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    CVS: $Id:$
 */

/**
 * Index class handles interaction on landing page
 * 
 * @author Sidrit Trandafili  
 */
class Admin extends Controller {

    /**
     * Instantiates the object and calls the constructor of the Parent class
     * 
     * @param
     * @return void 
     */
    function __construct() {
        parent::__construct();
        //Session::destroy();
    }


    function index(){

         $this->view->render('index', true);
    }

    function dashboard(){

         $this->view->render('dashboard', true);
    }

}