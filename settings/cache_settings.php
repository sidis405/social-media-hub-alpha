<?php

define('CACHE_BASE', $env_setup[ENV]['cache_base']);

define('INSTA_CACHE',  CACHE_BASE.'/cache/instagram');

define('INSTA_CACHE_PROFILE', INSTA_CACHE.'/profile');
define("INSTA_CACHE_PROFILE_AGE" , 60);


define('INSTA_CACHE_MEDIA', INSTA_CACHE.'/media');
define('INSTA_CACHE_DATA', INSTA_CACHE.'/data');

define("INSTA_CACHE_MEDIA_AGE" , 60);
define("INSTA_CACHE_DATA_AGE" , 60);

define('INSTA_CACHE_PROFILE_OUT', URL.'cache/instagram/profile/');
define('INSTA_CACHE_MEDIA_OUT', URL.'cache/instagram/media/');


define('TWITTER_CACHE',  CACHE_BASE.'/cache/twitter');

define('TWITTER_CACHE_PROFILE', TWITTER_CACHE.'/profile');
define("TWITTER_CACHE_PROFILE_AGE" , 60);


define('TWITTER_CACHE_MEDIA', TWITTER_CACHE.'/media');
define('TWITTER_CACHE_DATA', TWITTER_CACHE.'/data');
define("TWITTER_CACHE_MEDIA_AGE" , 60);
define("TWITTER_CACHE_DATA_AGE" , 60);


define('TWITTER_CACHE_PROFILE_OUT', URL.'cache/twitter/profile/');
define('TWITTER_CACHE_MEDIA_OUT', URL.'cache/twitter/media/');


define('FACEBOOK_CACHE',  CACHE_BASE.'/cache/facebook');

define('FACEBOOK_CACHE_PROFILE', FACEBOOK_CACHE.'/profile');
define("FACEBOOK_CACHE_PROFILE_AGE" , 60);


define('FACEBOOK_CACHE_MEDIA', FACEBOOK_CACHE.'/media');
define('FACEBOOK_CACHE_DATA', FACEBOOK_CACHE.'/data');
define("FACEBOOK_CACHE_MEDIA_AGE" , 60);
define("FACEBOOK_CACHE_DATA_AGE" , 60);


define('FACEBOOK_CACHE_PROFILE_OUT', URL.'cache/facebook/profile/');
define('FACEBOOK_CACHE_MEDIA_OUT', URL.'cache/facebook/media/');