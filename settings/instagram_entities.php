<?php

define('INSTA_TOKEN', '9996153.f59def8.4acf4fe085dd4381900284080aebdfab');


define('INSTA_ITEMS', serialize(array(

	'@TheYellowRome' => array(
			'label' => '@TheYellowRome',
			'value' => '196373511',
			'controller' => 'Users',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => '@TheYellowRome',
			'params' => array(
				'count' => 500
				),
			'icon' => 'icon-user'
			),

	'@TheYellowGuide' => array(
			'label' => '@TheYellowGuide',
			'value' => '192182649',
			'controller' => 'Users',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => '@TheYellowGuide',
			'params' => array(
				'count' => 500
				),
			'icon' => 'icon-user'
			),

	'theyellowrome' => array(
			'label' => '#theyellowrome',
			'value' => 'theyellowrome',
			'controller' => 'Tags',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => 'theyellowrome',
			'params' => array(
				'count' => 500
				),
			'icon' => 'icon-tag'
			),

	'theyellowguide' => array(
			'label' => '#theyellowguide',
			'value' => 'theyellowguide',
			'controller' => 'Tags',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => 'theyellowguide',
			'params' => array(
				'count' => 500
				)
			,
			'icon' => 'icon-tag'
			),

	'yellowhostel' => array(
			'label' => '#yellowhostel',
			'value' => 'yellowhostel',
			'controller' => 'Tags',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => 'yellowhostel',
			'params' => array(
				'count' => 500
				)
			,
			'icon' => 'icon-tag'
			),

	'yellowhostelrome' => array(
			'label' => '#yellowhostelrome',
			'value' => 'yellowhostelrome',
			'controller' => 'Tags',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => 'yellowhostelrome',
			'params' => array(
				'count' => 500
				)
			,
			'icon' => 'icon-tag'
			),

	'location_yellowrome' => array(
			'label' => 'TheYellowRome',
			'value' => '1538166',
			'controller' => 'Locations',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => 'location_yellowrome',
			'params' => array(
				'count' => 500
				),
			'icon' => 'icon-map-marker'
			),

	'location_yellowbar' => array(
			'label' => 'YellowBar',
			'value' => '447942',
			'controller' => 'Locations',
			'method' => 'Recent',
			'local_controller' => 'instagram/index',
			'search' => 'location_yellowbar',
			'params' => array(
				'count' => 500
				),
			'icon' => 'icon-map-marker'
			)

	)));