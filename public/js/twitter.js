
function toHashtagUrl(hashtag) {
  return "https://twitter.com/search?q=%23" + hashtag;
}


$(".storyUnit").linkify({
    hashtagUrlBuilder: toHashtagUrl, 
    target: "_blank"
});

