<?php


include('config.php');

define('CACHE',  URL.'cache/');

header('Content-type: image/png');

header("Cache-Control: private, max-age=10800, pre-check=10800");
header("Pragma: private");
header("Expires: " . date(DATE_RFC822,strtotime(" 30 day")));

if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
  // if the browser has a cached version of this image, send 304
  header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
  exit;
}


print file_get_contents(CACHE.base64_decode($_GET['data']));