<?php

	ini_set('display_errors',1); 
 	//error_reporting(E_ERROR);
	error_reporting(E_ALL);


set_include_path('/Users/sid/pear/share/pear');

include('settings/env_settings.php');


// Always provide a TRAILING SLASH (/) AFTER A PATH
define('URL', $env_setup[ENV]['url']); //Change on server change
define('LIBS', 'libs/'); //Library path

define('MODELS', 'models/'); //Library path

define('VIEWS', 'views/'); //

define('CONTROLLERS', 'controllers/'); //Library path

define('SOCIAL_BUTTONS', true);

define('TRACK', false);

define('SWIPE', true);

define('IMG', URL.'image.php?data=');

define('MEMCACHED_HOST', '127.0.0.1');
define('MEMCACHED_PORT', '11211');
define('MEMCACHED_USE', false);

include ('settings/twitter_entities.php');
include ('settings/facebook_entities.php');
include ('settings/instagram_entities.php');
include ('settings/cache_settings.php');

@define('ENTITIES', serialize(array(

	
	'Facebook' => array('text' => 'Facebook', 'icon' => 'F', 'data'=> FACEBOOK_ITEMS, 'main_controller' => 'facebook', 'icon-color'=>'icon-facebook-blue'),
	'Twitter' => array('text' => 'Twitter', 'icon' => 'T', 'data'=> TWITTER_ITEMS, 'main_controller' => 'twitter' ,'icon-color'=>'icon-twitter-blue'),
	'Instagram' => array('text' => 'Instagram', 'icon' => 'I', 'data'=> INSTA_ITEMS, 'main_controller' => 'instagram', 'icon-color'=>'icon-instagram-grey'),

	)));
